use std::fmt::Debug;

pub trait Objet: Debug {
    fn get_nom(&self) -> String;
    fn get_valeur(&self) -> usize;
}

#[derive(Debug, Clone, PartialEq)]
pub struct Tresor {
    nom: String,
    valeur: usize,
}

impl Tresor {
    /// Retourne un Tresor avec le nom et valeur fournies
    ///
    /// # Arguments
    ///
    /// * `nom` - Une String representant le nom du tresor
    ///
    /// * `valeur` - Un nombre positif representant la valeur du tresor
    ///
    /// # Examples
    ///
    /// ```
    /// use objets::*;
    /// let tresor = Tresor::new("graal", 2);
    /// ```
    pub fn new<S>(nom: S, valeur: usize) -> Self
    where
        S: Into<String>,
    {
        Tresor {
            nom: nom.into(),
            valeur,
        }
    }
}

impl Objet for Tresor {
    /// Retroune une String representant le nom du Tresor
    ///
    /// # Examples
    ///
    /// ```
    /// use objets::*;
    /// let tresor = Tresor::new("graal", 0);
    /// assert_eq! (tresor.get_nom(), "graal".to_string());
    /// ```
    fn get_nom(&self) -> String {
        self.nom.clone()
    }

    /// Retroune un usize representant la valeur du Tresor
    ///
    /// # Examples
    ///
    /// ```
    /// use objets::*;
    /// let tresor = Tresor::new("graal", 0);
    /// assert_eq! (tresor.get_valeur(), 0);
    /// ```
    fn get_valeur(&self) -> usize {
        self.valeur
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum ArmureType {
    Casque,
    Plastron,
    Bottes,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Armure {
    nom: String,
    valeur: usize,
    pub armure_type: ArmureType,
    pub effet: isize,
}

impl Armure {
    pub fn new_casque<S>(nom: S, valeur: usize, effet: isize) -> Self
    where
        S: Into<String>,
    {
        Armure {
            nom: nom.into(),
            valeur,
            armure_type: ArmureType::Casque,
            effet,
        }
    }

    pub fn new_plastron<S>(nom: S, valeur: usize, effet: isize) -> Self
    where
        S: Into<String>,
    {
        Armure {
            nom: nom.into(),
            valeur,
            armure_type: ArmureType::Plastron,
            effet,
        }
    }

    pub fn new_bottes<S>(nom: S, valeur: usize, effet: isize) -> Self
    where
        S: Into<String>,
    {
        Armure {
            nom: nom.into(),
            valeur,
            armure_type: ArmureType::Bottes,
            effet,
        }
    }
}

impl Objet for Armure {
    fn get_nom(&self) -> String {
        self.nom.clone()
    }

    fn get_valeur(&self) -> usize {
        self.valeur
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum ArmeType {
    Epee,
    Lance,
    Dague,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Arme {
    nom: String,
    valeur: usize,
    pub arme_type: ArmeType,
    pub effet: isize,
}

impl Objet for Arme {
    fn get_nom(&self) -> String {
        self.nom.clone()
    }

    fn get_valeur(&self) -> usize {
        self.valeur
    }
}

#[cfg(test)]
mod unittests_objets {
    use super::*;

    #[test]
    fn create_loot() {
        assert!(true);
    }
}
