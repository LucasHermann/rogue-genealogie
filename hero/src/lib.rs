use objets::{Arme, ArmeType, Armure, ArmureType, Objet};

pub trait Equipable: Objet {
    fn get_effet(&self) -> isize;
    fn equipe(self, equipement: &mut Equipement) -> Option<Box<dyn Equipable>>;
}

impl Equipable for Armure {
    fn get_effet(&self) -> isize {
        self.effet
    }

    fn equipe(self, equipement: &mut Equipement) -> Option<Box<dyn Equipable>> {
        match self.armure_type {
            ArmureType::Casque => {
                let precedent = equipement.tete.take();
                equipement.tete = Some(Box::new(self));
                precedent
            }
            ArmureType::Plastron => {
                let precedent = equipement.torse.take();
                equipement.torse = Some(Box::new(self));
                precedent
            }
            ArmureType::Bottes => {
                let precedent = equipement.pieds.take();
                equipement.pieds = Some(Box::new(self));
                precedent
            }
        }
    }
}

impl Equipable for Arme {
    fn get_effet(&self) -> isize {
        self.effet
    }

    fn equipe(self, equipement: &mut Equipement) -> Option<Box<dyn Equipable>> {
        match self.arme_type {
            ArmeType::Epee => {
                let precedent = equipement.main_gauche.take();
                equipement.main_gauche = Some(Box::new(self));
                precedent
            }
            ArmeType::Dague => {
                let precedent = equipement.main_gauche.take();
                equipement.main_gauche = Some(Box::new(self));
                precedent
            }
            ArmeType::Lance => {
                let precedent = equipement.main_gauche.take();
                equipement.main_gauche = Some(Box::new(self));
                precedent
            }
        }
    }
}

pub trait Personnage {
    fn attaque(&self) -> usize;
    fn defends(&mut self, degats: usize);
    fn get_vie(&self) -> usize;

    fn est_mort(&self) -> bool {
        self.get_vie() == 0
    }
}

#[derive(Default)]
pub struct Equipement {
    tete: Option<Box<dyn Equipable>>,
    torse: Option<Box<dyn Equipable>>,
    main_gauche: Option<Box<dyn Equipable>>,
    pieds: Option<Box<dyn Equipable>>,
}

pub struct Hero {
    nom: String,
    vie: usize,
    atk: usize,
    def: usize,
    equipement: Equipement,
    inventaire: Vec<Box<dyn Objet>>,
}

impl Hero {
    pub fn new<S>(nom: S, vie: usize, atk: usize, def: usize) -> Self
    where
        S: Into<String>,
    {
        Hero {
            nom: nom.into(),
            vie,
            atk,
            def,
            equipement: Default::default(),
            inventaire: Default::default(),
        }
    }
}

impl Personnage for Hero {
    fn attaque(&self) -> usize {
        self.atk
    }

    fn defends(&mut self, degats: usize) {
        if degats > self.def {
            if self.vie < (degats - self.def) {
                self.vie = 0;
            } else {
                self.vie -= degats - self.def;
            }
        }
    }

    fn get_vie(&self) -> usize {
        self.vie
    }
}

#[cfg(test)]
mod hero {
    use super::*;

    #[test]
    fn attaque() {
        let vie_test = 100;
        let atk_test = 10;
        let def_test = 0;

        let hero_test = Hero::new("test_hero", vie_test, atk_test, def_test);

        let result = hero_test.attaque();

        assert_eq!(atk_test, result);
    }

    #[test]
    fn defends_pas_def() {
        let vie_test = 100;
        let atk_test = 10;
        let def_test = 0;
        let degats_test = 10;

        let mut hero_test = Hero::new("test_hero", vie_test, atk_test, def_test);

        hero_test.defends(degats_test);

        assert_eq!(90, hero_test.vie);
    }

    #[test]
    fn defends_def_inf_a_degats() {
        let vie_test = 100;
        let atk_test = 10;
        let def_test = 5;
        let degats_test = 10;

        let mut hero_test = Hero::new("test_hero", vie_test, atk_test, def_test);

        hero_test.defends(degats_test);

        assert_eq!(95, hero_test.vie);
    }

    #[test]
    fn defends_def_sup_a_degats() {
        let vie_test = 100;
        let atk_test = 10;
        let def_test = 15;
        let degats_test = 10;

        let mut hero_test = Hero::new("test_hero", vie_test, atk_test, def_test);

        hero_test.defends(degats_test);

        assert_eq!(vie_test, hero_test.vie);
    }

    #[test]
    fn defends_degats_sup_a_vie() {
        let vie_test = 1;
        let atk_test = 10;
        let def_test = 0;
        let degats_test = 10;

        let mut hero_test = Hero::new("test_hero", vie_test, atk_test, def_test);

        hero_test.defends(degats_test);

        assert_eq!(0, hero_test.vie);
    }
}

#[cfg(test)]
mod amure {
    use super::*;

    fn assert_equipement<S, T>(sut: &Box<T>, exp_nom: S, exp_valeur: usize, exp_effet: isize)
    where
        S: Into<String>,
        T: Equipable + Objet + ?Sized,
    {
        assert_eq!(sut.get_nom(), exp_nom.into());
        assert_eq!(sut.get_valeur(), exp_valeur);
        assert_eq!(sut.get_effet(), exp_effet);
    }

    #[test]
    fn equipe_casque_equipement_vide() {
        let mut test_equipement: Equipement = Default::default();
        let test_casque_nom = "test casque nom";
        let test_casque_valeur = 0;
        let test_casque_effet = 0;
        let test_casque =
            Armure::new_casque(test_casque_nom, test_casque_valeur, test_casque_effet);

        let result = test_casque.equipe(&mut test_equipement);
        let result_casque = test_equipement.tete.take().unwrap();

        assert!(test_equipement.torse.is_none());
        assert!(test_equipement.main_gauche.is_none());
        assert!(test_equipement.pieds.is_none());

        assert!(result.is_none());

        assert_equipement(
            &result_casque,
            test_casque_nom,
            test_casque_valeur,
            test_casque_effet,
        );
    }

    #[test]
    fn equipe_casque_equipement_a_deja_casque() {
        let mut test_equipement: Equipement = Default::default();

        let test_casque_precedent_nom = "test casque precedent nom";
        let test_casque_precedent_valeur = 1;
        let test_casque_precedent_effet = 1;
        let test_casque_precedent = Armure::new_casque(
            test_casque_precedent_nom,
            test_casque_precedent_valeur,
            test_casque_precedent_effet,
        );

        let test_casque_nom = "test casque nom";
        let test_casque_valeur = 0;
        let test_casque_effet = 0;
        let test_casque =
            Armure::new_casque(test_casque_nom, test_casque_valeur, test_casque_effet);

        test_equipement.tete = Some(Box::new(test_casque_precedent));

        let mut result = test_casque.equipe(&mut test_equipement);
        let result_casque = test_equipement.tete.take().unwrap();

        assert!(test_equipement.torse.is_none());
        assert!(test_equipement.main_gauche.is_none());
        assert!(test_equipement.pieds.is_none());

        assert!(result.is_some());
        let result_casque_precedent = result.take().unwrap();

        assert_equipement(
            &result_casque,
            test_casque_nom,
            test_casque_valeur,
            test_casque_effet,
        );

        assert_equipement(
            &result_casque_precedent,
            test_casque_precedent_nom,
            test_casque_precedent_valeur,
            test_casque_precedent_effet,
        );
    }

    #[test]
    fn equipe_plastron_equipement_vide() {
        let mut test_equipement: Equipement = Default::default();
        let test_plastron_nom = "test plastron nom";
        let test_plastron_valeur = 0;
        let test_plastron_effet = 0;
        let test_plastron =
            Armure::new_plastron(test_plastron_nom, test_plastron_valeur, test_plastron_effet);

        let result = test_plastron.equipe(&mut test_equipement);
        let result_plastron = test_equipement.torse.take().unwrap();

        assert!(test_equipement.torse.is_none());
        assert!(test_equipement.main_gauche.is_none());
        assert!(test_equipement.pieds.is_none());

        assert!(result.is_none());

        assert_equipement(
            &result_plastron,
            test_plastron_nom,
            test_plastron_valeur,
            test_plastron_effet,
        );
    }

    #[test]
    fn equipe_plastron_equipement_a_deja_plastron() {
        let mut test_equipement: Equipement = Default::default();

        let test_plastron_precedent_nom = "test plastron precedent nom";
        let test_plastron_precedent_valeur = 1;
        let test_plastron_precedent_effet = 1;
        let test_plastron_precedent = Armure::new_plastron(
            test_plastron_precedent_nom,
            test_plastron_precedent_valeur,
            test_plastron_precedent_effet,
        );

        let test_plastron_nom = "test plastron nom";
        let test_plastron_valeur = 0;
        let test_plastron_effet = 0;
        let test_plastron =
            Armure::new_plastron(test_plastron_nom, test_plastron_valeur, test_plastron_effet);

        test_equipement.torse = Some(Box::new(test_plastron_precedent));

        let mut result = test_plastron.equipe(&mut test_equipement);
        let result_plastron = test_equipement.torse.take().unwrap();

        assert!(test_equipement.torse.is_none());
        assert!(test_equipement.main_gauche.is_none());
        assert!(test_equipement.pieds.is_none());

        assert!(result.is_some());
        let result_plastron_precedent = result.take().unwrap();

        assert_equipement(
            &result_plastron,
            test_plastron_nom,
            test_plastron_valeur,
            test_plastron_effet,
        );

        assert_equipement(
            &result_plastron_precedent,
            test_plastron_precedent_nom,
            test_plastron_precedent_valeur,
            test_plastron_precedent_effet,
        );
    }

    #[test]
    fn equipe_bottes_equipement_vide() {
        let mut test_equipement: Equipement = Default::default();
        let test_bottes_nom = "test bottes nom";
        let test_bottes_valeur = 0;
        let test_bottes_effet = 0;
        let test_bottes =
            Armure::new_bottes(test_bottes_nom, test_bottes_valeur, test_bottes_effet);

        let result = test_bottes.equipe(&mut test_equipement);
        let result_bottes = test_equipement.pieds.take().unwrap();

        assert!(test_equipement.torse.is_none());
        assert!(test_equipement.main_gauche.is_none());
        assert!(test_equipement.pieds.is_none());

        assert!(result.is_none());

        assert_equipement(
            &result_bottes,
            test_bottes_nom,
            test_bottes_valeur,
            test_bottes_effet,
        );
    }

    #[test]
    fn equipe_bottes_equipement_a_deja_bottes() {
        let mut test_equipement: Equipement = Default::default();

        let test_bottes_precedent_nom = "test bottes precedent nom";
        let test_bottes_precedent_valeur = 1;
        let test_bottes_precedent_effet = 1;
        let test_bottes_precedent = Armure::new_bottes(
            test_bottes_precedent_nom,
            test_bottes_precedent_valeur,
            test_bottes_precedent_effet,
        );

        let test_bottes_nom = "test bottes nom";
        let test_bottes_valeur = 0;
        let test_bottes_effet = 0;
        let test_bottes =
            Armure::new_bottes(test_bottes_nom, test_bottes_valeur, test_bottes_effet);

        test_equipement.pieds = Some(Box::new(test_bottes_precedent));

        let mut result = test_bottes.equipe(&mut test_equipement);
        let result_bottes = test_equipement.pieds.take().unwrap();

        assert!(test_equipement.torse.is_none());
        assert!(test_equipement.main_gauche.is_none());
        assert!(test_equipement.pieds.is_none());

        assert!(result.is_some());
        let result_bottes_precedent = result.take().unwrap();

        assert_equipement(
            &result_bottes,
            test_bottes_nom,
            test_bottes_valeur,
            test_bottes_effet,
        );

        assert_equipement(
            &result_bottes_precedent,
            test_bottes_precedent_nom,
            test_bottes_precedent_valeur,
            test_bottes_precedent_effet,
        );
    }
}
