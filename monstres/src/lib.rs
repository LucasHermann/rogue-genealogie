use objets::Objet;

pub struct Monstre {
    nom: String,
    vie: usize,
    atk: usize,
    def: usize,
    drop: Option<Box<dyn Objet>>,
}

impl Monstre {
    /// Créé un Mnstre avec le nom, vie, attaque, défense et drop fournis.
    ///
    /// # Arguments
    ///
    /// * `nom` - String représentant le nom du monstre.
    ///
    /// * `vie` - Usize représentant la vie du monstre.
    ///
    /// * `atk` - Usize représentant l'attaque du monstre.
    ///
    /// * `def` - Usize représentant la défense du monstre.
    ///
    /// * `drop` - Option représentant l'Objet que le monstre laisse à sa mort.
    ///
    /// # Examples
    ///
    /// ```
    /// use monstres::Monstre;
    ///
    /// let monstre = Monstre::new("Gobelin", 10, 1, 0, None);
    /// ```
    pub fn new<S>(nom: S, vie: usize, atk: usize, def: usize, drop: Option<Box<dyn Objet>>) -> Self
    where
        S: Into<String>,
    {
        Monstre {
            nom: nom.into(),
            vie,
            atk,
            def,
            drop,
        }
    }

    pub fn attaque(&self) -> usize {
        self.atk
    }

    fn defends(&mut self, degats: usize) {
        if degats > self.def {
            if self.vie < (degats - self.def) {
                self.vie = 0;
            } else {
                self.vie -= degats - self.def;
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
    }
}
