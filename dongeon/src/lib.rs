use hero::Personnage;
use objets::Objet;
use monstres::Monstre;

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Piege {
    degats: usize,
}

impl Piege {
    pub fn new(degats: usize) -> Self {
        Piege { degats }
    }

    pub fn get_degats(&self) -> usize {
        self.degats
    }
}

// #[derive(Debug, PartialEq)]
pub struct Salle {
    piege: Option<Piege>,
    monstre: Option<Monstre>,
    inventaire: Vec<Box<dyn Objet>>,
}

impl Salle {
    /// Créé une Salle avec le Piege, Monstre et Inventaire fournis.
    ///
    /// # Arguments
    ///
    /// * `piege` - Piege présent dans la Salle
    ///
    /// * `monstre` - Monstre présent dans la Salle
    ///
    /// * `inventaire` - Objets présents dans la Salle
    ///
    /// # Examples
    ///
    /// ```
    /// use dongeon::Salle;
    ///
    /// // Créé une Salle vide
    /// let salle = Salle::new(None, None, None);
    /// ```
    pub fn new(piege: Option<Piege>, monstre: Option<Monstre>, inventaire: Option<Vec<Box<dyn Objet>>>) -> Self {
        Salle {
            piege,
            monstre,
            inventaire: inventaire.unwrap_or_default(),
        }
    }

    /// Retourne un borrow du Piege de la Salle donnée.
    ///
    /// # Arguments
    ///
    /// # Examples
    ///
    /// ```
    /// use dongeon::*;
    ///
    /// // Créé une Salle vide
    /// let piege = Piege::new(10);
    /// let salle = Salle::new(Some(piege), None, None);
    /// let salle_piege = salle.get_piege();
    ///
    /// assert!(salle_piege.is_some());
    /// assert_eq!(salle_piege.unwrap(), piege);
    /// ```
    pub fn get_piege(&self) -> &Option<Piege> {
        &self.piege
    }

    /// Retourne un borrow du Monstre de la Salle donnée.
    ///
    /// # Arguments
    ///
    /// # Examples
    ///
    /// ```
    /// use dongeon::*;
    /// use monstres::Monstre;
    ///
    /// // Créé une Salle vide
    /// let monstre = Monstre::new("Gobelin", 10, 1, 0, None);
    /// let salle = Salle::new(None, Some(monstre), None);
    /// let salle_monstre = salle.get_monstre();
    ///
    /// assert!(salle_monstre.is_some());
    /// ```
    pub fn get_monstre(&self) -> &Option<Monstre> {
        &self.monstre
    }

    pub fn est_piegee(&self) -> bool {
        self.piege.is_some()
    }

    pub fn a_monstre(&self) -> bool {
        self.monstre.is_some()
    }
}

pub trait Tour {
    fn joue(&mut self);
    // fn tour_suivant(&self) -> Box<dyn Tour>;
}

pub struct Entre {
    hero: Box<dyn Personnage>,
    salle: Salle,
}

impl Entre {
    /// Créé un Tour de type Entre avec le Perosnnage et Salle fournis.
    ///
    /// # Arguments
    ///
    /// * `hero` - Hero entrant dans la Salle
    ///
    /// * `salle` - Salle où se passe le Tour
    ///
    /// # Examples
    ///
    /// ```
    /// use hero::Hero;
    /// use dongeon::{Entre, Salle};
    ///
    /// let hero = Hero::new("King Arthur", 100, 10, 10);
    /// let salle = Salle::new(None, None, None);
    /// let Entre = Entre::new(Box::new(hero), salle);
    /// ```
    pub fn new(hero: Box<dyn Personnage>, salle: Salle) -> Self {
        Entre {
            hero,
            salle,
        }
    }
}

impl Tour for Entre {
    fn joue(&mut self) {
        if self.salle.est_piegee() {
            let piege = self.salle.get_piege().expect("Failed to get Piege from trapped Salle.");
            self.hero.defends(piege.get_degats());
        }
    }
}

pub struct Combat;
pub struct Exploration;

#[cfg(test)]
mod tour {
    use super::*;

    struct MockHero {
        vie: usize,
        atk: usize,
        def: usize,
    }

    impl MockHero {
        fn new(vie: Option<usize>, atk: Option<usize>, def: Option<usize>) -> Self {
            MockHero {
                vie: vie.unwrap_or_default(),
                atk: atk.unwrap_or_default(),
                def: def.unwrap_or_default(),
            }
        }
    }

    impl Personnage for MockHero {
        fn attaque(&self) -> usize {
            0
        }

        fn defends(&mut self, degats: usize) {
            self.vie -= degats;
        }

        fn get_vie(&self) -> usize {
            self.vie
        }
    }

    #[test]
    fn salle_non_piegee() {
        let test_vie = 100;
        let test_hero = MockHero::new(Some(test_vie), None, None);
        let test_salle = Salle::new(None, None, None);
        let mut test_tour = Entre::new(Box::new(test_hero), test_salle);

        test_tour.joue();

        assert_eq!(test_tour.hero.get_vie(), test_vie);
    }

    #[test]
    fn salle_piegee() {
        let test_vie = 100;
        let test_degats = 10;
        let test_hero = MockHero::new(Some(test_vie), None, None);
        let test_piege = Piege::new(test_degats);
        let test_salle = Salle::new(Some(test_piege), None, None);
        let mut test_tour = Entre::new(Box::new(test_hero), test_salle);

        let expected_vie = test_vie - test_degats;

        test_tour.joue();

        assert_eq!(test_tour.hero.get_vie(), expected_vie);
    }
}

